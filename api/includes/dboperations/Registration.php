<?php

class Registration
{
	private $dbh;

	public function __construct(PDO $dbh)
	{
		$this->dbh = &$dbh;
	}
    
    

    public function saveAccount($password, $username,$email){
        $sql = "INSERT INTO `tbl_account` (`id`, `password`, `username`, `email`) VALUES (NULL, '$password', '$username', '$email');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }
    
    public function saveChannel($channel){
        $sql = "INSERT INTO `tbl_channel` (`id`, `channel`) VALUES (NULL, '$channel');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }
    
    public function saveFriend($user_id,$friend_id,$status){
        $sql = "INSERT INTO `tbl_friend` (`id`, `user_id`, `friend_id`, `status`) VALUES (NULL, '$user_id', '$friend_id', '$status');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }
    
    public function saveGlobalMessage($sub_channel_id,$user_id,$message,$date,$username){
        $sql = "INSERT INTO `tbl_global_message` (`id`, `sub_channel_id`, `user_id`, `username`, `message`, `date`) VALUES (NULL, '$sub_channel_id', '$user_id', '$username', '$message', '$date');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }
    
    
    public function savePrivateMessage($userId,$message,$date){
        $sql = "INSERT INTO `tbl_private_message` (`id`, `user_id`, `message`, `date`) VALUES (NULL, '$userId', '$message', '$date');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }
    
    public function saveSubChannel($channel_id,$name){
        $sql = "INSERT INTO `tbl_sub_channel` (`id`, `channel_id`, `name`) VALUES (NULL, '$channel_id', '$name');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }
    
    public function saveUserInfo($firstname,$middlename,$lastname,$age,$picture,$status){
        $sql = "INSERT INTO `tbl_user_info` (`id`, `first_name`, `middle_name`, `last_name`, `age`, `picture`, `status`) VALUES (NULL, '$firstname', '$middlename', '$lastname', '$age', '$picture', '$status');";
        var_dump($sql);
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->rowCount() ? $this->dbh->lastInsertId() : false;
    }

   

   
    
    
    // product list
    public function getAccounts(){
        $sql = "SELECT * from tbl_account";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    
    public function getFriend(){
        $sql = "SELECT * from tbl_friend";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getChannel(){
        $sql = "SELECT * from tbl_channel";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getGlobalMessage(){
        $sql = "SELECT * from tbl_global_message";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getPrivateMessage(){
        $sql = "SELECT * from tbl_private_message";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getSubChannel(){
        $sql = "SELECT * from tbl_sub_channel";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getUserInfo(){
        $sql = "SELECT * from tbl_user_info";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    // test
    public function getAccountInfo($user_id){
        $sql = "SELECT * FROM tbl_user_info a,tbl_account b where a.user_id = b.id and a.user_id = '".$user_id."' ";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function userLogin($username,$password){
        $sql = "SELECT * FROM `tbl_account` WHERE password = '$password' and username = '$username'";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getFriends($user_id){
        $sql = "SELECT * FROM `tbl_friend` WHERE user_id = '$user_id'";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getUserInfoFind($user_id){
        $sql = "SELECT * from tbl_user_info WHERE user_id = '$user_id'";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getGlobalChannelFind($sub_channel_id){
        $sql = "SELECT * from tbl_global_message WHERE sub_channel_id = '$sub_channel_id'";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getGlobalMessageFind($sub_channel_id){
        $sql = "SELECT * from tbl_global_message WHERE sub_channel_id = '$sub_channel_id'";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
       
}
?>
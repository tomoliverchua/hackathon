﻿using System.Collections;
using UnityEngine;

public enum SceneTransitionType
{
    LeftToRight,
    RightToLeft,
    FadeIn,
    FadeOut,
    FadeInFadeOut
}

public class SceneTransition : MonoBehaviour
{
 
    [SerializeField] private Animator animator;
    [SerializeField] private float AnimationDelay;

    public void PlayAnim(SceneTransitionType stt, System.Action callback)
    {
        switch (stt)
        {
            case SceneTransitionType.LeftToRight:
                animator.SetBool("LeftToRight", true);
      
                break;
            case SceneTransitionType.RightToLeft:
                animator.SetBool("RightToLeft", true);
             
                break;
            case SceneTransitionType.FadeInFadeOut:
       
                StartCoroutine(AnimWaitEvent(callback));
                break;
            default:
                break;
        }
    }

    IEnumerator AnimWaitEvent(System.Action callback)
    {
        if (animator.GetBool("FadeIn") == false && animator.GetBool("FadeOut") == false)
        {
            animator.SetBool("FadeIn", true);
            yield return new WaitForSeconds(AnimationDelay);
            animator.SetBool("FadeIn", false);
            if (callback != null)
                callback();
            yield return new WaitForSeconds(0.1f);
            animator.SetBool("FadeOut", true);
            Debug.Log("FadeOut");
            yield return new WaitForSeconds(.1f);
            animator.SetBool("FadeOut", false);
        }
        else
        {
            animator.SetBool("FadeIn", false);
            animator.SetBool("FadeOut", false);
        }
    }
}

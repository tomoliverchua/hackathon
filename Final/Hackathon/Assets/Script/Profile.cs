﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Profile : MonoBehaviour
{
    [SerializeField] private TMP_Text txtName;
    [SerializeField] private TMP_Text txtAge;
    [SerializeField] private TMP_Text txtFriend;
    [SerializeField] private Image pic;
    [SerializeField] private Sprite image1;
    [SerializeField] private Sprite image2;
    [SerializeField] private Sprite image3;
    [SerializeField] private Sprite image4;

    private void OnEnable()
    {
        UserInforModel model = AppManager.Instance.userInfoModel;
        txtName.text = $"{ model.firstname} {model.middlename}. {model.lastname}";
        txtAge.text = model.age.ToString();
        
      //  if(AppManager.Instance.AccountModel.ID )

        switch (AppManager.Instance.AccountModel.ID)
        {
            case "1":
                pic.sprite = AppManager.Instance.Images[0];
                break;
            case "2":
                pic.sprite = AppManager.Instance.Images[1];
                break;
            case "3":
                pic.sprite = AppManager.Instance.Images[2];
                break;
            default:
                pic.sprite = AppManager.Instance.Images[3];
                break;
        }

        
        if (AppManager.Instance.FriendModels == null)
        {
            txtFriend.text = "0";
            return;
        }


        txtFriend.text = AppManager.Instance.FriendModels.Count.ToString() ;
        
        
    }
}

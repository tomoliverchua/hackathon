﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Web : MonoBehaviour
{
    private const string server = "http://192.168.43.168/";

    #region Login
    public IEnumerator Login(string username, string password, System.Action<int> callBack)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/userlogin", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                // Network error
                callBack(-5);
            }
            else
            {
                string jsonArrayString = www.downloadHandler.text;

                var jSONArray = JSON.Parse(jsonArrayString);

                if (jSONArray[0].Value == "True")
                {
                    // Wrong credential
                    Debug.Log(www.downloadHandler.text);
                    callBack(-1);
                }
                else
                {
                    // Login Successful
                    // Get user account info
                    var ele = jSONArray[2];

                        
                    foreach (var item in ele)
                    {
                        Debug.Log(item.Value[  "id" ]);
                        StartCoroutine(GetAccountInfo(item.Value["id"]));
                    }

                    callBack(1);
                }
            }
        }
    }
    #endregion

    #region Friend

    public IEnumerator GetFriends(string userId, System.Action callBack = null)
    {

        WWWForm form = new WWWForm();
        form.AddField("user_id", userId);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/getFriendList", form))
        {
          
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();
           
           

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {

                string jsonArrayString = webRequest.downloadHandler.text;
                var jSONArray = JSON.Parse(jsonArrayString);

                //UserInforModel userInfo = new UserInforModel();
                //AccountModel account = new AccountModel();

                var elemensd = jSONArray[2];

                // Store friend 
                List<FriendModel> list = new List<FriendModel>();

                foreach (var info in elemensd)

                {   
                    Debug.Log(info.Value[0]);
                    FriendModel model = new FriendModel()
                    {
                        UserId = AppManager.Instance.AccountModel.ID,
                        FriendId = info.Value[2],
                        Status = info.Value[3]
                    };
                    
                    list.Add(model);            
                }
                AppManager.Instance.FriendModels = new List<FriendModel>();
                AppManager.Instance.FriendModels = list;

                foreach (var friend in list)
                {
                    StartCoroutine(GetUserInfo(friend.FriendId));
                }
               
            }
        }
        if (callBack != null)
            callBack();
    }

    #endregion

    #region UserInfo 
    public IEnumerator GetUserInfo(string userId, System.Action callBack = null)
    {

        WWWForm form = new WWWForm();
        form.AddField("user_id", userId);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/getUserInfoFind", form))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);             
            }
            else
            {

                string jsonArrayString = webRequest.downloadHandler.text;
                var jSONArray = JSON.Parse(jsonArrayString);

                var elemensd = jSONArray[2];

                // Store friend 
                Debug.Log(elemensd);
                List<UserInforModel> list = new List<UserInforModel>();
                foreach (var info in elemensd)
                {
                    UserInforModel model = new UserInforModel()
                    {
                        firstname = info.Value[2],
                        middlename = info.Value[3],
                        lastname = info.Value[4],
                        age = info.Value[5],
                        status = info.Value[7],
                    };
                 
                    list.Add(model);
                }
                AppManager.Instance.FriendInfo = list;
       
               if (callBack!= null)
                 callBack();
            }
        }
    }

    public IEnumerator GetOneUserInfo(string userId, System.Action<string> callBack = null)
    {

        WWWForm form = new WWWForm();
        form.AddField("user_id", userId);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/getUserInfoFind", form))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                string firstname = string.Empty;
                string jsonArrayString = webRequest.downloadHandler.text;
                var jSONArray = JSON.Parse(jsonArrayString);

                //UserInforModel userInfo = new UserInforModel();
                //AccountModel account = new AccountModel();

                var elemensd = jSONArray[2];
               
                // Store friend 
                Debug.Log(elemensd);
                List<UserInforModel> list = new List<UserInforModel>();
                foreach (var info in elemensd)
                {
                    firstname = info.Value[2];                               
                }
                AppManager.Instance.FriendInfo = list;

                if (callBack != null)
                    callBack(firstname);
            }
        }
    }
    #endregion

    #region Account

    public IEnumerator GetAccountInfo(string userId, System.Action<AccountModel> callBack = null)
    {

        WWWForm form = new WWWForm();
        form.AddField("user_id", userId);
        using (UnityWebRequest webRequest = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/getAccountInfo", form))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
                callBack(null);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);

                string jsonArrayString = webRequest.downloadHandler.text;
                var jSONArray = JSON.Parse(jsonArrayString);

                UserInforModel userInfo = new UserInforModel();
                AccountModel account = new AccountModel();

                var elemensd = jSONArray[2];
                foreach (var info in elemensd)
                {
                    userInfo.firstname = info.Value[2];
                    userInfo.middlename = info.Value[3];
                    userInfo.lastname = info.Value[4];
                    userInfo.age = info.Value[5];
                    userInfo.status = info.Value[7];
                    account.ID = info.Value[1];
                    account.Password = info.Value[8];
                    account.Username = info.Value[9];
                    account.Email = info.Value[10];
                }

                AppManager.Instance.AccountModel = account;
                AppManager.Instance.userInfoModel = userInfo;
              
            }
        }
    }

    #endregion

    #region privateChat
    public IEnumerator getChat(System.Action<List<PrivateMessageModel>> callBack = null)
    {
        
        using (UnityWebRequest www = UnityWebRequest.Get(server + "Hackathon/api/v1/index.php/getPrivateMessage"))
    {
        yield return www.SendWebRequest();
  
            if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            // Network error
        }
        else
            {
                // Login Successful

                Debug.Log(www.downloadHandler.text);

                string jsonArrayString = www.downloadHandler.text;

                var jSONArray = JSON.Parse(jsonArrayString);


                List<PrivateMessageModel> list = new List<PrivateMessageModel>();

                if (jSONArray.Count >= 2)
                {
                    var elemensd = jSONArray[2];

                    foreach (var item in elemensd)
                    {
                        PrivateMessageModel model = new PrivateMessageModel();
                        model.id = int.Parse(item.Value[0].Value);
                        model.userID = item.Value[1].Value;
                        model.message = item.Value[2].Value;
                        model.date = item.Value[3].Value;

                        list.Add(model);                       
                    }                  
                }
               if (callBack != null)
                callBack(list);
                
            }
        }
    }



    public IEnumerator SendChat(PrivateMessageModel model, System.Action callBack = null)
    {

        WWWForm form = new WWWForm();
        form.AddField("user_id", model.userID);
        form.AddField("message", model.message);
        form.AddField("date", model.date);

        using (UnityWebRequest www = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/savePrivateMessage", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                // Network error
            }
            else
            {
                if (www.downloadHandler.text.Contains("Wrong Credentials.") || www.downloadHandler.text.Contains("Username does not exists"))
                {
                    // Wrong credential
                    Debug.Log(www.downloadHandler.text);
                }
                else
                {
                    // Login Successful

                    Debug.Log(www.downloadHandler.text);

                }
            }
        }
    }
    #endregion

    #region globalChat
    public IEnumerator getGlobalChat(string globalID,System.Action<List<GlobalChannelModel>> callBack = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("sub_channel_id", globalID);
        using (UnityWebRequest www = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/getGlobalMessageFind",form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                // Network error
            }
            else
            {
                
                Debug.Log(www.downloadHandler.text);


                string jsonArrayString = www.downloadHandler.text;

                var jSONArray = JSON.Parse(jsonArrayString);


                List<GlobalChannelModel> list = new List<GlobalChannelModel>();
                if(jSONArray[2] != null) { 

                    var elemensd = jSONArray[2];

                    foreach (var item in elemensd)
                    {
                        GlobalChannelModel model = new GlobalChannelModel();
                        model.GlobalID = item.Value[1].Value;
                        model.UserID = item.Value[2].Value;
                        model.Username = item.Value[3].Value;
                        model.Message = item.Value[4].Value;
                        model.Date = item.Value[5].Value;

                        list.Add(model);                
                    }
                }
                else
                {
                    Debug.Log("No Message");
                }
                if (callBack != null)
                    callBack(list);

            }
        }
    }

    public IEnumerator sendGlobalChat(GlobalChannelModel model, System.Action callBack = null)
    {

        WWWForm form = new WWWForm();
        form.AddField("sub_channel_id", model.GlobalID);
        form.AddField("user_id", model.UserID);
        form.AddField("message", model.Message);
        form.AddField("date", model.Date);
        form.AddField("username", model.Username);

        using (UnityWebRequest www = UnityWebRequest.Post(server + "Hackathon/api/v1/index.php/saveGlobalMessage", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                // Network error
            }
            else
            {
                if (www.downloadHandler.text.Contains("Wrong Credentials.") || www.downloadHandler.text.Contains("Username does not exists"))
                {
                    // Wrong credential
                    Debug.Log(www.downloadHandler.text);
                }
                else
                {
                    // Login Successful

                    Debug.Log(www.downloadHandler.text);

                }
            }
        }
    }

    #endregion

    #region Test function
    public IEnumerator sample()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://192.168.0.22/Hackathon/api/v1/index.php/getPrivateMessage"))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                // Network error
            }
            else
            {
                if (www.downloadHandler.text.Contains("Wrong Credentials.") || www.downloadHandler.text.Contains("Username does not exists"))
                {
                    // Wrong credential
                    Debug.Log(www.downloadHandler.text);
                }
                else
                {
                    // Login Successful

                    Debug.Log(www.downloadHandler.text);

                    string jsonArrayString = www.downloadHandler.text;

                    var jSONArray = JSON.Parse(jsonArrayString);



                    var elemensd = jSONArray[2];


                    foreach (var item in elemensd)
                    {
                        foreach (var item2 in item.Value)
                        {
                            Debug.Log(item2.Value);
                        }
                    }
                }
            }
        }
    }

    IEnumerator GetSample3()
    {

        WWWForm form = new WWWForm();
        form.AddField("fname", "This is");
        form.AddField("mname", "From");
        form.AddField("lname", "Unity");

        using (UnityWebRequest webRequest = UnityWebRequest.Post("http://pillbentory.000webhostapp.com/PillInventory/api/v1/index.php/saveNewTest", form))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);
            }
        }
    }


    IEnumerator GetSample()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get("http://pillbentory.000webhostapp.com/PillInventory/api/v1/index.php/getProductList "))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);

                MessageBox.Instance.OkDialogue(webRequest.downloadHandler.text);
            }
        }
    }

    IEnumerator GetSample2()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get("http://pillbentory.000webhostapp.com/PillInventory/api/v1/index.php/saveReports"))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);
            }
        }
    }


    public IEnumerator GetDate(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
            }
        }
    }

    IEnumerator GetUser(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
            }
        }
    }



    IEnumerator RegisterUser(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("loginUser", username);
        form.AddField("loginPass", password);

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/UnityBackendTutorial/RegisterUser.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }

    public IEnumerator GetItemsID(string userID, System.Action<string> callBack)
    {

        WWWForm form = new WWWForm();
        form.AddField("userID", userID);

        using (UnityWebRequest webRequest = UnityWebRequest.Post("http://localhost/UnityBackendTutorial/GetItemsID.php", form))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();


            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);
                string jsonArray = webRequest.downloadHandler.text;
                callBack(jsonArray);
                // Call back function

            }
        }
    }

    public IEnumerator GetItem(string itemID, System.Action<string> callBack)
    {

        WWWForm form = new WWWForm();
        form.AddField("itemID", itemID);

        using (UnityWebRequest webRequest = UnityWebRequest.Post("http://localhost/UnityBackendTutorial/GetItem.php", form))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();


            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);
                string jsonArray = webRequest.downloadHandler.text;
                callBack(jsonArray);
                // Call back function

            }
        }
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class MoleGameManager : MonoBehaviour
{


    public GameObject Mole;
    public Transform holeParent;
    public Transform[] Holes;
    public float startTime;
    public float spawnTime;

    public TMP_Text scoreText;
    public TMP_Text timeText;

    private float timeBeforeSpawn;
    private float timeLeft;
    private int score;

    public bool isGameFinish;

    public GameObject StartingUI;

    private void OnEnable()
    {
        AppManager.Instance.SoundManager.Play("BGM_01");
        isGameFinish = true;
        StartingUI.SetActive(true);
        timeLeft = startTime;
        timeBeforeSpawn = spawnTime;
        score = 0;
        Holes = new Transform[holeParent.childCount];
        for (int i = 0; i < holeParent.childCount; i++)
        {
            Holes[i] = holeParent.GetChild(i);
        }
    }

    public void StartGame()
    {
        isGameFinish = false;
        timeLeft = startTime;
        timeBeforeSpawn = spawnTime;
        score = 0;
        Holes = new Transform[holeParent.childCount];
        for (int i = 0; i < holeParent.childCount; i++)
        {
            Holes[i] = holeParent.GetChild(i);
        }
        StartingUI.SetActive(false);
        AppManager.Instance.SoundManager.Play("Button_Click_01");
    }

    private void Update()
    {
        if (isGameFinish)
            return;

      
        if (timeLeft <= 0)
        {
            isGameFinish = true;
            StartingUI.SetActive(true);
            // Gameover
            return;
        }
        else
        {
            timeLeft -= Time.deltaTime;
        }
        if (Mole.activeSelf == false)
        {
            if (timeBeforeSpawn <= 0)
            {
                // Spawn mole
                int rand = Random.Range(0, Holes.Length);
                Mole.transform.SetParent(Holes[rand]);           
             
                timeBeforeSpawn = spawnTime;
                Mole.SetActive(true);
                Mole.transform.localPosition = new Vector3(0, 0);
            }
            else
            {
                timeBeforeSpawn -= Time.deltaTime;
            }
        }
        UpdateUI();
    }

    public void AddPoints()
    {
        Mole.SetActive(false);
        score++;
        timeLeft+= 3;
        UpdateUI();
    }

    public void RemoveTime()
    {
        Mole.SetActive(false);
        timeLeft -= 3;
        UpdateUI();
    }

    private void UpdateUI()
    {
        scoreText.text = score.ToString();
        timeText.text =  (timeLeft).ToString("00");
    }

    private void OnDisable()
    {
        AppManager.Instance.SoundManager.Stop("BGM_01");
    }

}

<?php

require_once '../config/database.php';
require_once '../includes/dboperations/Registration.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */

 function authenticate(\Slim\Route $route) {
 
     $headers = apache_request_headers();
     $response = array();
     $app = \Slim\Slim::getInstance();

     if (isset($headers['Authorization'])) {

 		$database = new Database();
         $conn = $database->getConnection();
         $db =  new DbOperation($conn);

         $api_key = $headers['Authorization'];
         // validating api key
         if (!$db->isValidApiKey($api_key)) {
             $response["error"] = true;
             $response["message"] = "Access Denied. Invalid Api key";
             echoResponse(401, $response);
             $app->stop();
         } else {
             global $user_id;
             $user_id = $db->getUserId($api_key);
         }
     } else {
         $response["error"] = true;
         $response["message"] = "Api key is misssing";
         echoResponse(400, $response);
         $app->stop();
     }
 }







// Endpoint /register is your endpoint
// $app->post or get();
//http://localhost/





// for stock in table
$app->post('/saveAddQuantiyReports', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('barcode','product_name','expiration_date','quantity','date_created','user','reference_number','box_no','lot_no','label'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $barcode = $app->request->post('barcode');
    $product_name = $app->request->post('product_name');
    $expiration_date = $app->request->post('expiration_date');
    $quantity = $app->request->post('quantity');
    $date_created = $app->request->post('date_created');
    $user = $app->request->post('user');
    $reference_number = $app->request->post('reference_number');
    $box_no = $app->request->post('box_no');
    $lot_no = $app->request->post('lot_no');
    $label = $app->request->post('label');
    
    $result = $db->saveAddQtyReports($barcode,$product_name,$expiration_date,$quantity,$date_created,$user,$reference_number,$box_no,$lot_no,$label);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});




// save data

$app->post('/saveAccount', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('password','username','email'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $username = $app->request->post('username');
    $password = $app->request->post('password');
    $email =  $app->request->post('email');

    $result = $db->saveAccount($username, $password, $email);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/saveChannel', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('channel'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $channel = $app->request->post('channel');


    $result = $db->saveChannel($channel);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/saveFriend', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('user_id','friend_id','status'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $user_id = $app->request->post('user_id');
    $friend_id = $app->request->post('friend_id');
    $status = $app->request->post('status');

    $result = $db->saveFriend($user_id,$friend_id,$status);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/saveGlobalMessage', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('sub_channel_id','user_id','message','date','username'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    $db = new Registration($conn);
    
    $response = array();
    
    $sub_channel_id = $app->request->post('sub_channel_id');
    $user_id = $app->request->post('user_id');
    $message = $app->request->post('message');
    $date = $app->request->post('date');
    $username = $app->request->post('username');

    $result = $db->saveGlobalMessage($sub_channel_id,$user_id,$message,$date,$username);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/savePrivateMessage', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('user_id','message','date'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    $db = new Registration($conn);
    
    $response = array();
    
    $userId = $app->request->post('user_id');
    $message = $app->request->post('message');
    $date = $app->request->post('date');
    

    $result = $db->savePrivateMessage($userId,$message,$date);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/savePrivateMessage', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('channel_id','name'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    $db = new Registration($conn);
    
    $response = array();
    
    $channel_id = $app->request->post('channel_id');
    $name = $app->request->post('name');
    

    $result = $db->saveSubChannel($sub_channel_id,$user_id,$message,$date);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/saveSubChannel', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('channel_id','name'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    $db = new Registration($conn);
    
    $response = array();
    
    $channel_id = $app->request->post('channel_id');
    $name = $app->request->post('name');
    

    $result = $db->saveSubChannel($sub_channel_id,$name);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});

$app->post('/saveSubChannel', function () use  ($app){
    
    //Required Parameters
    verifyRequiredParams(array('first_name','middle_name','last_name','age','picture','status'));
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    $db = new Registration($conn);
    
    $response = array();
    
    $first_name = $app->request->post('first_name');
    $middle_name = $app->request->post('middle_name');
    $last_name = $app->request->post('last_name');
    $age = $app->request->post('age');
    $picture = $app->request->post('picture');
    $status = $app->request->post('status');
    

    $result = $db->saveUserInfo($sub_channel_id,$name);
    
    if($result){
        $response['error'] = false;
        $response['message'] = "Successfully Registered";
    }
    else{
        $response['error'] = true;
        $response['message'] = "Failed to register";
    }
    echoResponse(200,$response);
});





















// get dta details
$app->get('/getAccounts', function () use  ($app){
    
 
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $result = $db->getAccounts();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});


$app->get('/getFriend', function () use  ($app){
    
 
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $result = $db->getFriend();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->get('/getChannel', function () use  ($app){
    
 
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $result = $db->getChannel();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->get('/getGlobalMessage', function () use  ($app){
    
 
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $result = $db->getGlobalMessage();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->get('/getPrivateMessage', function () use  ($app){
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $result = $db->getPrivateMessage();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->get('/getSubChannel', function () use  ($app){
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $result = $db->getSubChannel();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});
    
$app->get('/getUserInfo', function () use  ($app){
    
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();    
    
    $result = $db->getUserInfo();
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->post('/getAccountInfo', function () use  ($app){
    //Required Parameters
    verifyRequiredParams(array('user_id'));
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $id = $app->request->post('user_id');


    $result = $db->getAccountInfo($id);
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->post('/userlogin', function () use  ($app){
    //Required Parameters
    verifyRequiredParams(array('username','password'));
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $username = $app->request->post('username');
    $password = $app->request->post('password');

    $result = $db->userLogin($username,$password);
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = "Error!";
    }
    echoResponse(200,$response);
});

$app->post('/getFriendList', function () use  ($app){
    //Required Parameters
    verifyRequiredParams(array('user_id'));
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $user_id = $app->request->post('user_id');


    $result = $db->getFriends($user_id);
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = $result;
    }
    echoResponse(200,$response);
});

$app->post('/getUserInfoFind', function () use  ($app){
    //Required Parameters
    verifyRequiredParams(array('user_id'));
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $user_id = $app->request->post('user_id');


    $result = $db->getUserInfoFind($user_id);
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = $result;
    }
    echoResponse(200,$response);
});

$app->post('/getGlobalChannelFind', function () use  ($app){
    //Required Parameters
    verifyRequiredParams(array('sub_channel_id'));
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $sub_channel_id = $app->request->post('sub_channel_id');


    $result = $db->getGlobalChannelFind($sub_channel_id);
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = $result;
    }
    echoResponse(200,$response);
});

$app->post('/getGlobalMessageFind', function () use  ($app){
    //Required Parameters
    verifyRequiredParams(array('sub_channel_id'));
    //includes
    $database = new Database();
    $conn = $database->getConnection();
    
    //
    $db = new Registration($conn);
    
    $response = array();
    
    $sub_channel_id = $app->request->post('sub_channel_id');


    $result = $db->getGlobalMessageFind($sub_channel_id);
    
     if($result){
        $response['error'] = false;
        $response['message'] = "Fetched!";
        foreach ($result as $key => $value) {
            $tmp[$key] = $value;
        }
        $response['details'] = $tmp;
    }
    else{
        $response['error'] = true;
        $response['message'] = $result;
    }
    echoResponse(200,$response);
});







/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields)
{
    $error = false;
    $error_fields = "";
    $request_params = $_REQUEST;

    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }

    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoResponse(400, $response);
        $app->stop();
    }
}

$app->run();
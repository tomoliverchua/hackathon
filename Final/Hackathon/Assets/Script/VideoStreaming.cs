﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoStreaming : MonoBehaviour
{

    public RawImage rawImage;
    public VideoPlayer videoPlayer;

    // Start the video
    public void StartVideo()
    {
        StartCoroutine(AppManager.Instance.Web.getGlobalChat("1"));
        StartCoroutine(PlayVideo());
        videoPlayer.loopPointReached += CheckOver;
    }

    // Wait for the video to load
    IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        while (!videoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(1f);
        }
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
    }
    // Check when the video is done
    void CheckOver(VideoPlayer vp)
    {
        videoPlayer.loopPointReached -= CheckOver;
        gameObject.SetActive(false);
    }

    // Stop the video
    public void Click_Stop()
    {
        if (videoPlayer.isPlaying)
        {
            videoPlayer.frame = (long)videoPlayer.frameCount;
        }
    }

    public bool CheckMovie()
    {
        if (videoPlayer.isPlaying)
            return true;
        else
            return false;
    }
}

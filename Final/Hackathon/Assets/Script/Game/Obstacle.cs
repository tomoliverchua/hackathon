﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float speed;
    public int Damage = 1;

    public GameObject effect;

    private void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            Instantiate(effect, transform.position, Quaternion.identity);
            // Hit the player
            collision.GetComponent<PlayerMovement>().PlayerLife -= Damage;
            Destroy(gameObject);
        }
    }
}

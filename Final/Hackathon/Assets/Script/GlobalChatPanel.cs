﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GlobalChatPanel : MonoBehaviour
{
    public TMP_Text txtUsername;
    public TMP_Text txtMessage;
    public Image pic;

    private GlobalChannelModel model = new GlobalChannelModel();

    public void setPrivateMessage(string id, string userId, string message, string date, string username)
    {
        model.GlobalID = id;
        model.UserID = userId;
        model.Message = message;
        model.Date = date;
        model.Username = username;
        txtUsername.text = username;
        txtMessage.text = message;

        switch (userId)
        {
            case "1":
                pic.sprite = AppManager.Instance.Images[0];
                break;
            case "2":
                pic.sprite = AppManager.Instance.Images[1];
                break;
            case "3":
                pic.sprite = AppManager.Instance.Images[2];
                break;
            default:
                pic.sprite = AppManager.Instance.Images[3];
                break;
        }

    }
}

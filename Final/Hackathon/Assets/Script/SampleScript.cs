﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleScript : MonoBehaviour
{

    public GameObject loginUI;
    public GameObject dashboardUI;
    public GameObject profileUI;
    public GameObject friendUI;
    public GameObject channelUI;
    public GameObject subchannelUI;
    public GameObject Game1;



    public void Login()
    {
        System.Action tempFunction = () => {
            loginUI.SetActive(false);
            dashboardUI.SetActive(true);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    
    }

    public void Logout()
    {
        System.Action tempFunction = () => {
            loginUI.SetActive(true);
            dashboardUI.SetActive(false);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have

    }

    public void Profile()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(false);
            profileUI.SetActive(true);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }

    public void Profile_Back()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(true);
            profileUI.SetActive(false);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }

    public void Friend()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(false);
            friendUI.SetActive(true);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }

    public void Friend_Back()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(true);
            friendUI.SetActive(false);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }

    public void Channel()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(false);
            channelUI.SetActive(true);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }
    public void Channel_Back()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(true);
            channelUI.SetActive(false);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }

    public SubChannel subChannel;


    public void SubChannel()
    {
        System.Action tempFunction = () => {
            channelUI.SetActive(false);
            subchannelUI.SetActive(true);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }
    public void SubChannel_Back()
    {
        System.Action tempFunction = () => {
            dashboardUI.SetActive(true);
            subchannelUI.SetActive(false);
        };

        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        // Play Sound if you have
    }


    public GameObject camera1;
    public GameObject camera2;
    public GameObject gamePanel1;
    public GameObject fullPanel;
    public GameObject halfPanel;
    public GameObject scrollView;

    public void OpenGame()
    {
        Game1.SetActive(true);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);
        gamePanel1.SetActive(true);
        HalfView();
    }

    public void CloseGame()
    {
        Game1.SetActive(false);
        gamePanel1.SetActive(false);

        fullPanel.SetActive(true);
        halfPanel.SetActive(false);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);
        scrollView.transform.SetParent(fullPanel.transform);
    }

    public void FullView()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(false);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);     
    }
    public void HalfView()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(true);
        camera1.gameObject.SetActive(false);
        camera2.gameObject.SetActive(true);
        scrollView.transform.SetParent(halfPanel.transform);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
    }
    
    public void Half_Camera()
    {
        mainCamera.rect = new Rect(0, 0.5f, 1, .05f);
    }

}

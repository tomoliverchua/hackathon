﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class FriendPanel : MonoBehaviour
{
    [SerializeField] private TMP_Text txtName;
    [SerializeField] private Image pic;
    [SerializeField] private Sprite image1;
    [SerializeField] private Sprite image2;
    [SerializeField] private Sprite image3;
    [SerializeField] private Sprite image4;

    private string userID;
    private UserInforModel _model;
    GameObject privateMessageUI;

    private void Start()
    {
        privateMessageUI = AppManager.Instance.PrivateMessageUI;
    }

    public void SetInfo(UserInforModel model, string id)
    {
         userID = id;
        _model = model;
        txtName.text = $"{model.firstname} {model.middlename}. {model.lastname}";

        switch (userID)
        {
            case "1":
                pic.sprite = AppManager.Instance.Images[0];
                break;
            case "2":
                pic.sprite = AppManager.Instance.Images[1];
                break;
            case "3":
                pic.sprite = AppManager.Instance.Images[2];
                break;
            default:
                pic.sprite = AppManager.Instance.Images[3];
                break;
        }

    }

    public void Click_Friend()
    {
        try
        {
            privateMessageUI.SetActive(true);
            privateMessageUI.GetComponent<PrivateMessage>().SetUp(_model, userID);
            AppManager.Instance.FriendUI.SetActive(false);
        }
        catch (Exception ex)
        {
            MessageBox.Instance.OkDialogue(ex.Message);
        }

    }
}

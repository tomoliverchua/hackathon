﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameChannel : MonoBehaviour
{
    public GameObject ChannelUI;
    public GameObject Game1;
    public GameObject Game2;

    public GameObject gameListUI;
    public GameObject camera1;
    public GameObject camera2;
    public GameObject gamePanel;
    public GameObject fullPanel;
    public GameObject halfPanel;

    public TMP_Text title;

    public GameObject prefab1;
    public GameObject prefab2;
    public TMP_InputField input;
    public GameObject parent;
    public ScrollRect scroll;
    private bool isBottom;


    public UserInforModel UserInfor { get; set; }

    List<GlobalChannelModel> listofMessage = new List<GlobalChannelModel>();

    Action<List<GlobalChannelModel>> globalMessageCallback;

    private UserInforModel _model;
    private string _globalID;

    private void OnEnable()
    {
        globalMessageCallback = (status) =>
        {
            // put your method here 
            globalMessageMethod(status);
        };
        InvokeRepeating("checkForChat", 3f, 3f);
    }

    public void SetUp(string ChannelName, string globalID)
    {

        _globalID = globalID;

        title.text = ChannelName;
        InitChat();
    }

    private void checkForChat()
    {
        InitChat();
    }


    private void globalMessageMethod(List<GlobalChannelModel> status)
    {
        try
        {
            listofMessage = status;
            if (listofMessage == null)
                return;
            if (listofMessage.Count == 0)
                return;

            if (status == null)
            {
                return;
            }

            if (parent.transform.childCount == listofMessage.Count)
                return;
            if (parent.transform.childCount > 0)
            {
                foreach (Transform child in parent.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
            foreach (var item in listofMessage)
            {
              

                if (item.UserID == AppManager.Instance.AccountModel.ID)
                {
                    GameObject temp = Instantiate(prefab2, transform.position, Quaternion.identity);
                    temp.transform.SetParent(parent.transform);
                    temp.transform.localScale = Vector3.one;
                    temp.GetComponent<GlobalChatPanel>().setPrivateMessage(item.GlobalID, item.UserID, item.Message, item.Date, item.Username);
                }
                else
                {
                    GameObject temp = Instantiate(prefab1, transform.position, Quaternion.identity);
                    temp.transform.SetParent(parent.transform);
                    temp.transform.localScale = Vector3.one;
                    temp.GetComponent<GlobalChatPanel>().setPrivateMessage(item.GlobalID, item.UserID, item.Message, item.Date, item.Username);
                }
            }
        }
        catch (Exception ex)
        {
            MessageBox.Instance.OkDialogue(ex.Message);
        }
    }

    public void SendChat()
    {
        if (string.IsNullOrEmpty(input.text))
            return;
        GlobalChannelModel model = new GlobalChannelModel();
        model.GlobalID = _globalID;
        model.UserID = AppManager.Instance.AccountModel.ID;
        model.Message = input.text;
        model.Username = AppManager.Instance.userInfoModel.firstname;
        model.Date = DateTime.UtcNow.ToShortDateString().ToString();
        StartCoroutine(AppManager.Instance.Web.sendGlobalChat(model));
        isBottom = false;
        input.text = "";
        StartCoroutine(delay());
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(2f);
        if (!isBottom)
        {
            isBottom = true;
            scroll.normalizedPosition = new Vector2(0, 0);
        }
    }


    public void Click_Back()
    {
        CancelInvoke("checkForChat");
        ChannelUI.SetActive(true);
        gameObject.SetActive(false);
        if (_globalID == "10" || _globalID == "11" || _globalID == "12")
        {
            CloseVideo();

        }
    }

    public void InitChat()
    {
        StartCoroutine(AppManager.Instance.Web.getGlobalChat(_globalID,globalMessageCallback));
    }


    public void Click_Game1()
    {
        if (Game1.activeSelf != true)
        {
            Game1.SetActive(true);
            Game2.SetActive(false);
            camera1.gameObject.SetActive(true);
            camera2.gameObject.SetActive(false);
            gamePanel.SetActive(true);
            HalfView();
        }
    }
    public void Click_Game2()
    {
        if (Game2.activeSelf != true)
        {
            Game1.SetActive(false);
            Game2.SetActive(true);
            camera1.gameObject.SetActive(true);
            camera2.gameObject.SetActive(false);
            gamePanel.SetActive(true);
            HalfView();
        }
    }
    public void Click_GameListToogle()
    {
        gameListUI.SetActive(!gameListUI.activeSelf);
    }
    public void CloseGame()
    {
        Game1.SetActive(false);
        Game2.SetActive(false); 
        gamePanel.SetActive(false);

        fullPanel.SetActive(true);
        halfPanel.SetActive(false);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);
        scroll.transform.SetParent(fullPanel.transform);
    }


    public void FullView()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(false);
        gameListUI.SetActive(false);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);
    }
    public void HalfView()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(true);
        camera1.gameObject.SetActive(false);
        camera2.gameObject.SetActive(true);
        scroll.transform.SetParent(halfPanel.transform);
    }

    public GameObject videoUI;
    public GameObject videoPanel;
    public GameObject halfScreenParent;
    public GameObject fullScreenParent;

    public void CloseVideo()
    {

        videoUI.GetComponent<VideoStreaming>().Click_Stop();
        videoUI.SetActive(false);
        videoPanel.SetActive(false);
        fullScreenParent.SetActive(true);
        halfScreenParent.SetActive(false);
        fullPanel.SetActive(true);
        halfPanel.SetActive(false);
        scroll.transform.SetParent(fullPanel.transform);
    }


    public void FullscreenVideo()
    {
        videoUI.GetComponent<VideoStreaming>().StartVideo();
        fullPanel.SetActive(false);
        halfPanel.SetActive(false);
        fullScreenParent.SetActive(true);
        halfScreenParent.SetActive(false);
        videoUI.transform.SetParent(fullScreenParent.transform);
    }
    public void HalfScreenVideo()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(true);
        fullScreenParent.SetActive(false);
        halfScreenParent.SetActive(true);
        scroll.transform.SetParent(halfPanel.transform);
        videoUI.transform.SetParent(halfScreenParent.transform);
    }

    public void Click_Movie()
    {
        if (videoUI.GetComponent<VideoStreaming>().CheckMovie())
        {
            CloseVideo();
        }
        else
        {
            halfScreenParent.SetActive(true);
            videoUI.SetActive(true);
            videoUI.GetComponent<VideoStreaming>().StartVideo();
            HalfView();
        }
            //videoPanel.SetActive(true);
    }


    public void Click_temp()
    {
        MessageBox.Instance.OkDialogue("Temporary Unavailable...\n For future development");
    }


    public GameObject newsUI;


    public void CloseNews()
    {
        newsUI.SetActive(false);
        videoPanel.SetActive(false);
        fullScreenParent.SetActive(true);
        halfScreenParent.SetActive(false);
        fullPanel.SetActive(true);
        halfPanel.SetActive(false);
        scroll.transform.SetParent(fullPanel.transform);
    }


    public void FullscreenNews()
    {
        newsUI.SetActive(true);
        fullPanel.SetActive(false);
        halfPanel.SetActive(false);
        fullScreenParent.SetActive(true);
        halfScreenParent.SetActive(false);
        newsUI.transform.SetParent(fullScreenParent.transform);
    }
    public void HalfScreenNews()
    {
        newsUI.SetActive(true);
        fullPanel.SetActive(false);
        halfPanel.SetActive(true);
        fullScreenParent.SetActive(false);
        halfScreenParent.SetActive(true);
        scroll.transform.SetParent(halfPanel.transform);
        newsUI.transform.SetParent(halfScreenParent.transform);
    }

    public void Click_News()
    {
        videoPanel.SetActive(true);
        halfScreenParent.SetActive(true);
            newsUI.SetActive(true);
        HalfScreenNews();
        //videoPanel.SetActive(true);
    }














    private void OnDisable()
    {
        if (videoUI != null)
        {
            if (videoUI.activeSelf == true)
            {
                if (videoUI.GetComponent<VideoStreaming>().CheckMovie())
                {
                    CloseVideo();
                }
            }

        }
        CloseGame();
        if (parent.transform.childCount > 0)
        {
            foreach (Transform child in parent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        StopCoroutine(AppManager.Instance.Web.getGlobalChat(_globalID, globalMessageCallback));
    }
}

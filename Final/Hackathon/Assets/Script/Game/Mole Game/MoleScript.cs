﻿using UnityEngine;

public class MoleScript : MonoBehaviour
{

    public MoleGameManager MoleGameManager;
    private Collider2D coll;

    public float TimeToRemove;
    public float TimeLeft;

    private void Start()
    {
        coll = gameObject.GetComponent<Collider2D>();
        TimeLeft = TimeToRemove;
    }


    private void OnEnable()
    {
        TimeLeft = TimeToRemove;
    }
    public void Update()
    {
        if (MoleGameManager.isGameFinish)
            return;
        if (TimeLeft <= 0)
        {
            MoleGameManager.RemoveTime();
        }
        else
        {
            TimeLeft -= Time.deltaTime;
        }

    }

    private void OnMouseDown()
    {
        if (MoleGameManager.isGameFinish)
            return;
        MoleGameManager.AddPoints();
        AppManager.Instance.SoundManager.Play("Button_Click_01");
    }

}

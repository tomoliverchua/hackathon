﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Channel : MonoBehaviour
{

 
    [SerializeField] private GameObject gamePanelUI;
    [SerializeField] private GameObject newsPanelUI;
    [SerializeField] private GameObject shoppingPanelUI;
    [SerializeField] private GameObject MoviePanelUI;
    [SerializeField] private GameObject GameUI;
    [SerializeField] private GameObject MovieUI;
    [SerializeField] private GameObject NewsUI;
    [SerializeField] private GameObject ShoppingUI;


    public void Click_Game(bool i)
    {
        gamePanelUI.SetActive(i);
      
    }
    public void Click_OpenGameChannel(int globalID)
    {
        Debug.Log("sdfsdfdsfsdfsd");
        if (globalID == 0)
            return;
        GameUI.SetActive(true);
        gameObject.SetActive(false);
        GameUI.GetComponent<GameChannel>().SetUp("Global Messages"+"(Game)", globalID.ToString());
    }

    public void Click_OpenMovieChannel(int globalID)
    {

        Debug.Log("sdfsdfdsfsdfsd");
        if (globalID == 0)
            return;
        MovieUI.SetActive(true);
        gameObject.SetActive(false);
        MovieUI.GetComponent<GameChannel>().SetUp("Global Messages" + "(Movie)", globalID.ToString());
    }


    public void Click_OpenNewsChannel(int globalID)
    {
        //Application.OpenURL("https://www.google.com/");

        Debug.Log("sdfsdfdsfsdfsd");
        if (globalID == 0)
            return;
        NewsUI.SetActive(true);
        gameObject.SetActive(false);
        NewsUI.GetComponent<GameChannel>().SetUp("Global Messages" + "(News)", globalID.ToString());
    }

    public void Click_OpenShoppingChannel(int globalID)
    {
        Debug.Log("sdfsdfdsfsdfsd");
        if (globalID == 0)
            return;
        ShoppingUI.SetActive(true);
        gameObject.SetActive(false);
        ShoppingUI.GetComponent<GameChannel>().SetUp("Global Messages" + "(Shopping)", globalID.ToString());
    }

    public void Click_News(bool i)
    {
        newsPanelUI.SetActive( i);
    }
    public void Click_Shopping(bool i)
    {
        shoppingPanelUI.SetActive(i);
    }
    public void Click_Movies(bool i)
    {
        MoviePanelUI.SetActive(i);
    }

    private void OnDisable()
    {
        gamePanelUI.SetActive(false);
        newsPanelUI.SetActive(false);
        shoppingPanelUI.SetActive(false);
        MoviePanelUI.SetActive(false);
}
}

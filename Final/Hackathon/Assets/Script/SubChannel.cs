﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SubChannel : MonoBehaviour
{
    public TMP_Text Title;

    public void SetInfo(string title, int gameNo )
    {
        Title.text = title;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrivateMessageModel
{
    public int id { get; set; }
    public string userID { get; set; }
    public string message { get; set; }
    public string date { get; set; }
}

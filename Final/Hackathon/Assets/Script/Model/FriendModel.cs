﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FriendModel
{
    public string UserId { get; set; }
    public string FriendId { get; set; }
    public string Status { get; set; }
}

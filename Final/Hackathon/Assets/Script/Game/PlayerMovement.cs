﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Vector2 targetPos = new Vector2(-2, 0);
    [SerializeField] private float speed;
    [SerializeField] private float maxHeight;
    [SerializeField] private float minHeight;

    public int PlayerLife;
    public float yMovespeed;

    private void OnEnable()
    {
        AppManager.Instance.SoundManager.Play("BGM_02");
    }

    private void Update()
    {
        if (PlayerLife <= 0)
        {
            // Game over
        }
        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.UpArrow) && transform.position.y < maxHeight)
        {
            targetPos = new Vector2(transform.position.x, transform.position.y + yMovespeed);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && transform.position.y > minHeight)
        {
            targetPos = new Vector2(transform.position.x, transform.position.y - yMovespeed);
        }
    }

    private void OnDisable()
    {
        AppManager.Instance.SoundManager.Stop("BGM_02");
    }

}

﻿[System.Serializable]
public class GlobalChannelModel 
{
    public string GlobalID { get; set; }
    public string UserID { get; set; }
    public string Username { get; set; }
    public string Message { get; set; }
    public string Date { get; set; }
}

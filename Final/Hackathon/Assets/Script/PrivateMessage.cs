﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PrivateMessage : MonoBehaviour
{
    public GameObject DashBoardUI;
    public GameObject Game1;
    public GameObject Game2;

    public GameObject gameListUI;
    public GameObject camera1;
    public GameObject camera2;
    public GameObject gamePanel;
    public GameObject fullPanel;
    public GameObject halfPanel;

    public TMP_Text title;

    public GameObject prefab1;
    public GameObject prefab2;
    public TMP_InputField input;
    public GameObject parent;
    public ScrollRect scroll;
    private bool isBottom;
    

    public UserInforModel UserInfor { get; set; }

    List<PrivateMessageModel> listofMessage = new List<PrivateMessageModel>();

    Action<List<PrivateMessageModel>> privateMessageCallBack;

    private UserInforModel _model;
    private string _friendId;

    private void OnEnable()
    {
        privateMessageCallBack = (status) =>
        {
            // put your method here 
            privateMessageMethod(status);
        };
        InvokeRepeating("checkForChat", 3f, 3f);
    }

    public void SetUp(UserInforModel model, string friendId)
    {

            
        _model = model;
        _friendId = friendId;

        title.text = model.firstname;
            InitChat();
        StartCoroutine(delay());

    }

    private void checkForChat()
    {
        InitChat();
    }


    private void privateMessageMethod(List<PrivateMessageModel> status)
    {
        try
        {
            listofMessage = status;
            if (listofMessage == null)
                return;
            if (listofMessage.Count == 0)
                return;

            if (status == null)
            {
                return;
            }

            if (parent.transform.childCount == listofMessage.Count)
                return;
            if (parent.transform.childCount > 0)
            {
                foreach (Transform child in parent.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
            foreach (var item in listofMessage)
            {
                //if (item.userID == UserID)
                //{
                //    GameObject temp = Instantiate(prefab1, transform.position, Quaternion.identity);
                //    temp.transform.SetParent(parent.transform);
                //    temp.transform.localScale = Vector3.one;
                //    temp.GetComponent<PrivateMessagePanel>().setPrivateMessage(item.id, item.userID, item.message, item.date);
                //}
                ////else if (item.userID == AppManager.Instance.AccountModel.ID)
                //else if (item.userID == "2")
                //{
                //    GameObject temp = Instantiate(prefab2, transform.position, Quaternion.identity);
                //    temp.transform.SetParent(parent.transform);
                //    temp.transform.localScale = Vector3.one;
                //    temp.GetComponent<PrivateMessagePanel>().setPrivateMessage(item.id, _model.firstname, item.message, item.date);
                //}


                if (item.userID == AppManager.Instance.AccountModel.ID)
                {
                    GameObject temp = Instantiate(prefab2, transform.position, Quaternion.identity);
                    temp.transform.SetParent(parent.transform);
                    temp.transform.localScale = Vector3.one;
                    temp.GetComponent<PrivateMessagePanel>().setPrivateMessage(item.id, item.userID, item.message, item.date);
                }
                else
                {
                    GameObject temp = Instantiate(prefab1, transform.position, Quaternion.identity);
                    temp.transform.SetParent(parent.transform);
                    temp.transform.localScale = Vector3.one;
                    temp.GetComponent<PrivateMessagePanel>().setPrivateMessage(item.id, item.userID, item.message, item.date);
                }
            }
          
        }
        catch (Exception ex)
        {
            MessageBox.Instance.OkDialogue(ex.Message);
        }
    }

    public void SendChat()
    {
        if (string.IsNullOrEmpty(input.text))
            return;
        PrivateMessageModel model = new PrivateMessageModel();

        model.userID = AppManager.Instance.AccountModel.ID;
        model.message = input.text;
        model.date = DateTime.UtcNow.ToShortDateString().ToString();
        StartCoroutine(AppManager.Instance.Web.SendChat(model));
        isBottom = false;
        input.text = "";
        StartCoroutine(delay());
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(3f);
        if (!isBottom)
        {
            isBottom = true;
            scroll.normalizedPosition = new Vector2(0, 0);
        }
    }
   

    public void Click_Back()
    {
        CancelInvoke("checkForChat");
        CloseGame();
        DashBoardUI.SetActive(true);
        gameObject.SetActive(false);
    }

    public void InitChat()
    {
        StartCoroutine(AppManager.Instance.Web.getChat(privateMessageCallBack));
       
    }
    public void Click_Game1()
    {
        if (Game1.activeSelf != true)
        {
            Game1.SetActive(true);
            Game2.SetActive(false);
            camera1.gameObject.SetActive(true);
            camera2.gameObject.SetActive(false);
            gamePanel.SetActive(true);
            HalfView();
        }
    }
    public void Click_Game2()
    {
        if (Game2.activeSelf != true)
        {
            Game1.SetActive(false);
            Game2.SetActive(true);
            camera1.gameObject.SetActive(true);
            camera2.gameObject.SetActive(false);
            gamePanel.SetActive(true);
            HalfView();
        }
    }
    public void Click_GameListToogle()
    {
        gameListUI.SetActive(!gameListUI.activeSelf);
    }
    public void CloseGame()
    {
        Game1.SetActive(false);
        Game2.SetActive(false);
        gamePanel.SetActive(false);

        fullPanel.SetActive(true);
        halfPanel.SetActive(false);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);
        scroll.transform.SetParent(fullPanel.transform);
    }


    public void FullView()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(false);
        gameListUI.SetActive(false);
        camera1.gameObject.SetActive(true);
        camera2.gameObject.SetActive(false);
    }
    public void HalfView()
    {
        fullPanel.SetActive(false);
        halfPanel.SetActive(true);
        camera1.gameObject.SetActive(false);
        camera2.gameObject.SetActive(true);
        scroll.transform.SetParent(halfPanel.transform);
    }

    private void OnDisable()
    {
        CloseGame();
    }
}

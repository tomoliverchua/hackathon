﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserInforModel
{
    public string firstname { get; set; }
    public string middlename { get; set; }
    public string lastname { get; set; }
    public int age { get; set; }
    public bool status { get; set; }
}

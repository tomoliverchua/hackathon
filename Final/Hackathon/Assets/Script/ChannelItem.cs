﻿using UnityEngine;

public class ChannelItem : MonoBehaviour
{
    
    [SerializeField] private GameObject ItemContent;


    public void Click_Header()
    {
        ItemContent.SetActive(!ItemContent.activeSelf);
    }
}

﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AccountInfo 
{
    public int userId { get; set; }
    public string username { get; set; }
    public string password { get; set; }
    public string email { get; set; }

    public string firstname { get; set; }
    public string middlename { get; set; }
    public string lastname { get; set; }
    public int age { get; set; }
    public bool status { get; set; }
    public UnityEngine.Texture picture { get; private set; }


    public void ConvertPic(string pic)
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    #region Singleton class
    public static AppManager Instance { get; set; }

    private void Awake()
    {
        // Make sure only 1 instance of this class exist in the game
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else { Destroy(gameObject); }
    }
    #endregion

    public SceneTransition SceneTransition;
    public Web Web;
    public SoundManager SoundManager;

    public AccountModel AccountModel;
    public UserInforModel userInfoModel;
    public List<FriendModel> FriendModels;
    public List<UserInforModel> FriendInfo;
    public GameObject PrivateMessageUI;
    public GameObject FriendUI;

    public Sprite[] Images;
}

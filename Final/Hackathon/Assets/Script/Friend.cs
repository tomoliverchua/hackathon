﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Friend : MonoBehaviour
{
    // prefabs
    [SerializeField] private GameObject friendPanel;
    // prefabs parent
    [SerializeField] private GameObject parent;
    [SerializeField] private TMP_InputField usernameInput;

    Action FriendCallBack;
    private void OnEnable()
    {

            FriendCallbackMethod();
    }

    private void FriendCallbackMethod()
    {
        if (parent.transform.childCount > 0)
        {
            foreach (Transform child in parent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        for (int i = 0; i < AppManager.Instance.FriendInfo.Count; i++)
        {
            GameObject temp = Instantiate(friendPanel, transform.position, Quaternion.identity);
            temp.transform.SetParent(parent.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<FriendPanel>().SetInfo(AppManager.Instance.FriendInfo[i], AppManager.Instance.FriendModels[i].FriendId);
        }

        //foreach (var friend in AppManager.Instance.FriendInfo)
        //{
        //    GameObject temp = Instantiate(friendPanel, transform.position, Quaternion.identity);
        //    temp.transform.SetParent(parent.transform);
        //    temp.transform.localScale = Vector3.one;
        //    temp.GetComponent<FriendPanel>().SetInfo(friend);
        //}
    }

    public void Click_AddFriend()
    {

        MessageBox.Instance.OkDialogue("Not available...\n Work in progress...");
        //if (string.IsNullOrEmpty(usernameInput.text) || string.IsNullOrWhiteSpace(usernameInput.text))
        //{
        //    MessageBox.Instance.OkDialogue("Input valid data.");
        //    return;
        //}

        // TODO:  sdfsdf

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Login : MonoBehaviour , IMessageModel
{
    [SerializeField] private TMP_InputField userInput;
    [SerializeField] private TMP_InputField passwordInput;
    [SerializeField] private GameObject loginUI;
    [SerializeField] private GameObject dashboardUI;


    Action<int> LoginUserCallBack;

    private void Start()
    {
        LoginUserCallBack = (status) =>
        {
            // put your method here 
            loginCallbackMethod(status);
        };
    }

    private void loginCallbackMethod(int loginStatus)
    {
        switch (loginStatus)
        {
            case -5:
                MessageBox.Instance.OkDialogue("Network problem...");
                break;
            case -1:
                MessageBox.Instance.OkDialogue("Invalid username/password...");
                break;
            case 1:
                MessageBox.Instance.OkDialogue("Login Successful...", this);
                break;
            default:
                break;
        }
    }

    public Web web;
     
    public void Click_Login()
    {
        
        // Check if input empty
        if (string.IsNullOrEmpty(userInput.text) || string.IsNullOrEmpty(passwordInput.text))
        {
            // Open Mesagebox
            MessageBox.Instance.OkDialogue("Invalid username/password");
            return;
        } 
        StartCoroutine( web.Login(userInput.text, passwordInput.text, LoginUserCallBack));


    }

    public void OkMessagge()
    {
        System.Action tempFunction = () => {
            loginUI.SetActive(false);
            dashboardUI.SetActive(true);
        };
        userInput.text = "";
        passwordInput.text = "";
        AppManager.Instance.SceneTransition.PlayAnim(SceneTransitionType.FadeInFadeOut, tempFunction);

        Debug.Log(AppManager.Instance.AccountModel.Username);
        Debug.Log(AppManager.Instance.userInfoModel.firstname);
    }
}

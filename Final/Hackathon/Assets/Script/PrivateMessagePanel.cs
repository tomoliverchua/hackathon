﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PrivateMessagePanel : MonoBehaviour
{
    public TMP_Text username;
    public TMP_Text message;
    public Image pic;

    private PrivateMessageModel privateMessageModel = new PrivateMessageModel();

    public void setPrivateMessage(int id, string userId, string message, string date)
    {
        privateMessageModel.id = id;
        privateMessageModel.userID = userId;
        privateMessageModel.message = message;
        privateMessageModel.date = date;

        username.text = userId.ToString();
        this.message.text = message;


        switch (userId)
        {
            case "1":
                pic.sprite = AppManager.Instance.Images[0];
                break;
            case "2":
                pic.sprite = AppManager.Instance.Images[1];
                break;
            case "3":
                pic.sprite = AppManager.Instance.Images[2];
                break;
            default:
                pic.sprite = AppManager.Instance.Images[3];
                break;
        }

    }




}
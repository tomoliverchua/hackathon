﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SampleChat : MonoBehaviour
{

    public GameObject prefab1;
    public GameObject prefab2;
    public TMP_InputField input;
    public GameObject parent;

    List<PrivateMessageModel> listofMessage = new List<PrivateMessageModel>();

    public string UserID;

    Action<List<PrivateMessageModel>> privateMessageCallBack;

    private void Start()
    {
        privateMessageCallBack = (status) =>
        {
            // put your method here 
            privateMessageMethod(status);
        };

       

        InvokeRepeating("checkForChat", 3f, 3f);
    }

    private void checkForChat()
    {
   
       
        InitChat();    
    }

    private void privateMessageMethod(List<PrivateMessageModel> status)
    {

        listofMessage = status;
        if (listofMessage == null)
            return;
        if (listofMessage.Count == 0)
            return;

        if (status == null)
        {
            return;
        }
     
        if (parent.transform.childCount == listofMessage.Count)
            return;
        if (parent.transform.childCount > 0)
        {
            foreach (Transform child in parent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        foreach (var item in listofMessage)
        {
            if (item.userID == "1")
            {
                GameObject temp = Instantiate(prefab1, transform.position, Quaternion.identity);
                temp.transform.SetParent(parent.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<PrivateMessagePanel>().setPrivateMessage(item.id, item.userID, item.message, item.date);
            }
            else if (item.userID == "2")
            {
                GameObject temp = Instantiate(prefab2, transform.position, Quaternion.identity);
                temp.transform.SetParent(parent.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<PrivateMessagePanel>().setPrivateMessage(item.id, item.userID, item.message, item.date);
            }
        }
    }


    public void SendChat()
    {
        if (string.IsNullOrEmpty(input.text))
            return;
        PrivateMessageModel model = new PrivateMessageModel();

        model.userID = UserID;
        model.message = input.text;
        model.date = DateTime.UtcNow.ToShortDateString().ToString();
        StartCoroutine(AppManager.Instance.Web.SendChat(model));
    }

    
    public void InitChat()
    {
        StartCoroutine(AppManager.Instance.Web.getChat(privateMessageCallBack));
    }
}

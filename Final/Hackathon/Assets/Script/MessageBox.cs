﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public interface IMessageModel
{
    void OkMessagge();
}

public class MessageBox : MonoBehaviour
{

    #region Singleton
    /// <summary>
    /// Main game Manager Singleton Class
    /// </summary>
    public static MessageBox Instance { get; set; }
    /// <summary>
    /// Run when the script instance is being loaded
    /// </summary>
    private void Awake()
    {
        // Make sure only 1 instance of this class exist in the game
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else { Destroy(gameObject); }
    }
    #endregion

    public TMP_Text okMessage;
    public GameObject OkMessageUI;

    IMessageModel _model;

    public void OkDialogue(string message, IMessageModel model = null)
    {
        OkMessageUI.SetActive(true);
        okMessage.text = message;
        _model = model;
    }


    public void Ok()
    {
        if (_model != null)
            _model.OkMessagge();
        _model = null;
        OkMessageUI.SetActive(false);
    }
}
